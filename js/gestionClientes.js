var clientesObtenidos;
var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";

function getClientes(){
  //var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      //console.log(request.responseText);
      clientesObtenidos=request.responseText;
      procesarClientes();
    }
  }
  request.open("GET",url,true);
  request.send();
}

//Nombre, ciudad, telefono y pais

function procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);

  var divTabla = document.getElementById("divTabla");
  var tabla = document.createElement("table");
  var tbody= document.createElement("tbody");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for(var i=0; i<JSONClientes.value.length; i++){
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    var columnaCiudad = document.createElement("td");
    var columnaTelefono = document.createElement("td");
    var columnaCountry = document.createElement("td");
    var columnaBandera = document.createElement("td");
    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");

    columnaNombre.innerText=JSONClientes.value[i].ContactName;
    nuevaFila.append(columnaNombre);
    columnaCiudad.innerText=JSONClientes.value[i].City;
    nuevaFila.append(columnaCiudad);
    columnaTelefono.innerText=JSONClientes.value[i].Phone;
    nuevaFila.append(columnaTelefono);
    columnaCountry.innerText=JSONClientes.value[i].Country;
    nuevaFila.append(columnaCountry);

    if(JSONClientes.value[i].Country =="UK"){
      imgBandera.src = rutaBandera+"United-Kingdom.png";
    }else {
      imgBandera.src = rutaBandera+JSONClientes.value[i].Country+".png";
    }
    columnaBandera.appendChild(imgBandera);

    nuevaFila.append(columnaBandera);


    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.append(tabla);
}

function obtenerBandera(bandera){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      //console.log(request.responseText);
      clientesObtenidos=request.responseText;
      procesarClientes();
    }
  }
  request.open("GET",url,true);
  request.send();
}

//function bandera(){
  //var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-United-Kingdom.png":
  //var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"+""+".png":
//}
